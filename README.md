## Website used for the online study

To run the website, you need to lauch a local server.

We recommend to use the `http-server` package. You can install it by typing `npm install http-server -g` in your terminal.

you can then launch the server by typing `http-server` at the root of the project.

You can then access the website by following the link http://127.0.0.1:8080/form.html.

NB : please note that little adjustements have been made when translating the form from its original language to english and preparing it for submission. 