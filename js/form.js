
// Constants

const alpha = Array.from(Array(21)).map((e, i) => i + 65);
const alphabet = alpha.map((x) => String.fromCharCode(x));
const actions = ["tap", "swipe", "flex", "hold"];
const action_types = ["D", "S"];

setTimeout(function(){        
  $('#loader-wrapper').fadeOut();
  $('#loader-wrapper').delay(250).fadeOut('slow'); 
}, 7500);


// Randomizer
function shuffleTabs() {
  handleSeed("static", "dynamic", getRandomInt(100), false);
  handleSeed("action", "order", getRandomInt(100), true);
  
  for (const action of actions) {
    for (const action_type of action_types) {
      handleSeed(action, action_type, getRandomInt(100), false);
    }
  }
}

shuffleTabs();

function handleSeed(partOne, partTwo, seed, sameSeed) {
  writeSeed(partOne, partTwo, seed);
  shuffleSeed(partOne, partTwo, seed, sameSeed);
}

function writeSeed(partOne, partTwo, seed) {
  for (var element of document.getElementsByName("seed_"+partOne+"_"+partTwo).values()) {
    element.value = seed;
  }
}

function filterArray(array) {
  var filtered = array.filter(function(el) { return el.nodeName != "#text"; }).map(function(el) { return el.id});
  return filtered;
}

function writeArray(partOne, partTwo, array) {
  var filtered = filterArray(array);
  for (var element of document.getElementsByName("array_"+partOne+"_"+partTwo).values()) {
    element.value = filtered.join('_');
  }
}

function shuffleSeed(partOne, partTwo, seed, sameSeed) {
  var container_div = document.getElementsByName("shuffle_"+partOne+"_"+partTwo);
  if (!sameSeed) {
    var chance_seed = new Chance(seed);
  }

  for (var div of container_div) {
    if (sameSeed) {
      var chance_seed = new Chance(seed);
    }
    var array = chance_seed.shuffle(div.childNodes);
    writeArray(partOne, partTwo, array)
    replaceNodeListWithArray(div, array);
  }
}

// Fill questions

function fillQuestions() {
  for (const action of actions) {
    for (const action_type of action_types) {
      for (const letter of alphabet) {
        templateQuestion(action, letter, action_type);
      }
      handleGeneralModal(action, action_type);
    }
  }
}

fillQuestions();

function templateQuestion(action, letter, type) {  
  var type_extension = "jpg"; 
  if (type=="D") {
    type_extension = "gif";
  }
  var div = document.createElement('div');
  div.setAttribute('class', 'form-row col-sm-6');
  div.innerHTML = document.getElementById('question-template').innerHTML;
  div.innerHTML = div.innerHTML
      .replace(/{ACTION}/g, action)
      .replace(/{LETTER}/g, letter)
      .replace(/{TYPE}/g, type)
      .replace(/{TYPE_EXTENSION}/g, type_extension);
  var element = action + '-' + letter + type;
  document.getElementById(element).appendChild(div);
  handleModal(action, letter, type);
}

function handleModal(action, letter, type) {
  var str = "{ACTION}_{LETTER}{TYPE}".replace(/{ACTION}/g, action)
                              .replace(/{LETTER}/g, letter)
                              .replace(/{TYPE}/g, type);
  
  var btn = document.getElementById("comment_btn_"+str);
  var modal = document.getElementById("comment_modal_"+str);
  var span = document.getElementById("span_"+str);

  btn.onclick = function() {
    modal.style.display = "block";
  }
  span.onclick = function() {
    modal.style.display = "none";
  }
}

function handleGeneralModal(action, type) {
  var str = "{ACTION}_{TYPE}".replace(/{ACTION}/g, action)
                              .replace(/{TYPE}/g, type);
  
  var btn = document.getElementById("comment_btn_"+str);
  var modal = document.getElementById("comment_modal_"+str);
  var span = document.getElementById("span_"+str);

  btn.onclick = function() {
    modal.style.display = "block";
  }
  span.onclick = function() {
    modal.style.display = "none";
  }
}

function addNumbers() {
  var labels = document.getElementsByClassName("overlapping-label");
  var nbr = 0;
  for (var label of labels) {
    nbr+=1;
    label.innerHTML = ('0' + nbr).slice(-2);
    if (nbr>=21) {
      nbr=0;
    }
  }
}

addNumbers();

// Random fill questions

function randomAnswerRadioGroups() {
  var radio_familiarity_mg = document.getElementsByName("familiarity_mg");
  radio_familiarity_mg[getRandomInt(radio_familiarity_mg.length-1)+1].checked=true;
  var radio_familiarity_ar = document.getElementsByName("familiarity_ar");
  radio_familiarity_ar[getRandomInt(radio_familiarity_ar.length-1)+1].checked=true;

  for (const action of actions) {
    for (const letter of alphabet) {
      for (const action_type of action_types) {
        var radio = document.getElementsByName("mg_"+action+"_"+letter+action_type);
        radio[getRandomInt(radio.length-1)+1].checked=true;
      }
    }
  }
}


$('form').submit(function(e) {
  e.preventDefault();
  $.ajax({
       type: 'POST',
       url: '',
       data: $(this).serialize(),
       complete: redirectSuccess(),
      //  success: redirectSuccess()
  });
})

function randomAnswerCheckboxes() {
  var checkboxes = Array.from(document.getElementsByTagName("input"));
  checkboxes = checkboxes.filter(function(el) { return el.type =="checkbox"; });
  var chance_seed = new Chance(getRandomInt(100));
  checkboxes = chance_seed.shuffle(checkboxes);
  const nbr_checked = getRandomInt(checkboxes.length-1);
  var i = 0;
  while (i<nbr_checked) {
    checkboxes[i+1].checked = true;
    i++;
  }
}

// randomAnswerRadioGroups();
// randomAnswerCheckboxes();

function redirect() {
  window.top.location.href="http://127.0.0.1:8080/hci-form-end-failure";
}
function redirectSuccess() {
  window.top.location.href="http://127.0.0.1:8080/hci-form-end-success";
}

// Step Form
var currentTab = 0;
showTab(currentTab);
function showTab(n) {
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  if (n == (x.length - 1)) {
    document.getElementById('submitBtn').style.display="inline";
    document.getElementById('nextBtn').style.display="none";
  } else {
    document.getElementById('submitBtn').style.display="none";
    document.getElementById('nextBtn').style.display="inline";
  }
  fixStepIndicator(n)
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  document.getElementById('nextBtnButton').innerHTML="Next";
  currentTab = currentTab + n;
  showTab(currentTab);
  $('html, body').animate({ scrollTop: 0 }, 'fast');
}

function validateRadioGroups(action, action_type) {
  var valid = validateFirstRadios();
  var temp_valid;
  if (action=="blank" || action_type=="blank") return valid;
  for (const letter of alphabet) {
    temp_valid = validateRadioGroup(action, letter, action_type);
    if (temp_valid==false) {
      valid=false;
    }
  }
  return valid;
}

function validateFirstRadios() {
  var familiarity_mg = document.getElementsByName("familiarity_mg");
  var familiarity_ar = document.getElementsByName("familiarity_ar");
  var valid_mg=false;
  var valid_ra=false;

  for (var i = 0; i < familiarity_mg.length; i++) {
    if (familiarity_mg[i].checked) {
        valid_mg = true;
        break;
    }
  }
  for (var i = 0; i < familiarity_ar.length; i++) {
    if (familiarity_ar[i].checked) {
        valid_ra = true;
        break;
    }
  }

  console.log(familiarity_mg[0]);
  var parentClasses = familiarity_mg[0].classList;
  if (valid_mg) {
    parentClasses.remove("unanswered-div");
    parentClasses.add("answered-div");
  } else {
    parentClasses.add("unanswered-div");
    parentClasses.remove("answered-div");
  }

  var parentClasses = familiarity_ar[0].classList;
  if (valid_ra) {
    parentClasses.remove("unanswered-div");
    parentClasses.add("answered-div");
  } else {
    parentClasses.add("unanswered-div");
    parentClasses.remove("answered-div");
  }
  return valid_mg && valid_ra;
}

function validateRadioGroup(action, letter, action_type) {
  var radio = document.getElementsByName("mg_"+action+"_"+letter+action_type);
  var valid = false;
  for (var i = 0; i < radio.length; i++) {
    if (radio[i].checked) {
        valid = true;
        break;
    }
  }

  var parentClasses = radio[0].classList;
  if (valid) {
    parentClasses.remove("unanswered-div");
    parentClasses.add("answered-div");
  } else {
    parentClasses.add("unanswered-div");
    parentClasses.remove("answered-div");
  }
  return valid;
}

function answerRadio(radio) {
  var superDivClasses = radio.parentElement.classList;
  if (superDivClasses.contains("unanswered-div")) {
    superDivClasses.remove("unanswered-div");
    superDivClasses.add("answered-div");
  }
} 


function validateForm() {
  var x, y, i, valid = true;
  validFormLabel = document.getElementById("validFormLabel");
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");

  if (currentTab==0) {
    return true;
  }
  valid = validateRadioGroups(x[currentTab].dataset.action, x[currentTab].dataset.action_type);
  if (!valid) {
    validFormLabel.innerHTML = "Please answer all the questions";
    validFormLabel.style.opacity = 100;
    validFormLabel.style.filter = "alpha(opacity=100)";
    validFormLabel.style.display = "block";
    validFormLabel.style.visibility = "visible";
    customFadeOut(validFormLabel, 2500);
  }

  for (i = 0; i < y.length; i++) {
    if (y[i].required && y[i].type!="radio" && y[i].value == "") {
      y[i].className += " invalid";
      validFormLabel.innerHTML = y[i].name + " is not valid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  } 
  return valid;
  
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}

// FormTools

function customFadeOut( elem, ms )
{
  if( ! elem )
    return;

  if( ms )
  {
    var opacity = 1;
    var timer = setInterval( function() {
      opacity -= 50 / ms;
      if( opacity <= 0 )
      {
        clearInterval(timer);
        opacity = 0;
        elem.style.display = "none";
        elem.style.visibility = "hidden";
      }
      elem.style.opacity = opacity;
      elem.style.filter = "alpha(opacity=" + opacity * 100 + ")";
    }, 50 );
  }
  else
  {
    elem.style.opacity = 0;
    elem.style.filter = "alpha(opacity=0)";
    elem.style.display = "none";
    elem.style.visibility = "hidden";
  }
}

function showComment() {

  document.getElementsByName('textInput').type="text";
}

function getRandomInt(max_not_included) {
  return Math.floor(Math.random() * max_not_included);
}

function replaceNodeListWithArray(container, arrayOfNodes) {
  removeAllChildNodes(container);
  arrayOfNodes.forEach(function(item){
    container.appendChild(item);
  });
};


function removeAllChildNodes(parent) {
  while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
  }
}